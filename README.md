- [ ] Add `AbstractReplayBuffer`
- [ ] Similarly to [`MinimalRLCore`](https://github.com/mkschleg/MinimalRLCore.jl/blob/master/src/episode.jl),
  add `AbstractEpisode` and `Episode`, you can probably just copy-and-cite that
  file! This will make experiments much easier.
