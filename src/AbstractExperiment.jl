"""
    AbstractExperiment

Implements any experiment which can run a learning algorithm (algo) on an environment
"""
abstract type AbstractExperiment end

"""
    AbstractExperimentEnder

Implements functionality for ending an experiment
"""
abstract type AbstractExperimentEnder end

"""
    AbstractHook

Anything that can track data during an experiment
"""
abstract type AbstractHook end

"""
    AbstractExperimentSaver end

Implements functionality for saving experimental data
"""
abstract type AbstractExperimentSaver end

"""
    run!(a::AbstractExperiment)

Runs the experiment
"""
function run!(a::AbstractExperiment)
    error("run not implemented for AbstractExperiment $(typeof(a))")
end

"""
    run_episode!(a::AbstractExperiment)::Int

Runs a single episode of the experiment and returns the number of steps the episode took
"""
function run_episode!(a::AbstractExperiment)::Int
    error("run_episode not implemented for AbstractExperiment $(typeof(a))")
end

function close!(exp::AbstractExperiment)
    close!(env(exp))
    close!(algo(exp))

    for time in instances(CallTime)
        for hook in hooks(exp)[time]
            close!(hook, exp)
        end
    end

    expclose!(exp)
end

"""
    expclose!(exp::AbstractExperiment)

Cleanup experiment resources *besides* the environment, algorithm, and experiment hooks
"""
function expclose!(exp::AbstractExperiment)
    error("expclose! not implemented for AbstractExperiment of type $(typeof(exp))")
end

"""
    data(a::AbstractExperiment)

Gets the data generated by the experiment to disk
"""
function data(a::AbstractExperiment)::NamedTuple
    error("data not implemented for AbstractExperiment $(typeof(a))")
end

"""
    register!(a::AbstractExperiment, t::AbstractHook)

Registers a new tracker with the experiment.
"""
function register!(a::AbstractExperiment, t::AbstractHook)
    error("register not implemented for AbstractExperiment $(typeof(a)) and " *
        "AbstractHook $(typeof(t))")
end

function register!(a::AbstractExperiment, hooks::AbstractHook...)
    for hook in hooks
        register!(a, hook)
    end
end

"""
    env(a::AbstractExperiment)::AbstractEnvironment

Returns the environment that the experiment is run on
"""
function env(a::AbstractExperiment)::AbstractEnvironment
    error("env not implemented for AbstractExperiment $(typeof(a))")
end

"""
    algo(a::AbstractExperiment)::Abstractalgo

Returns the algo that the experiment is run with
"""
function algo(a::AbstractExperiment)::Abstractalgo
    error("algo not implemented for AbstractExperiment $(typeof(a))")
end

"""
    hooks(e::AbstractExperiment)::Dict{String,AbstractHook}

Returns the dictionary of hooks registered with the experiment
"""
function hooks(e::AbstractExperiment)::Dict{CallTime, AbstractHook}
    error("hooks not implemented for AbstractExperiment $(typeof(a))")
end

"""
    total_episodes(e::AbstractExperiment)::Int

Returns the total number of episodes finished in the experiment so far
"""
function total_episodes(e::AbstractExperiment)::Int
    error("total_episodes not implemented for AbstractExperiment $(typeof(a))")
end

"""
    total_steps(e::AbstractExperiment)::Int

Returns the total number of steps taken in the experiment so far
"""
function total_steps(e::AbstractExperiment)::Int
    error("total_steps not implemented for AbstractExperiment $(typeof(a))")
end

"""
    ended(a::AbstractExperimentEnder, e::AbstractExperiment)::Bool

Returns whether or not the experiment should be considered finished. If `true`, then the
experiment should be ended
"""
function ended(a::AbstractExperimentEnder, e::AbstractExperiment)::Bool
    error("end not implemented for AbstractExperimentEnder $(typeof(t))")
end

"""
    (t::AbstractHook)(::AbstractExperiment)

Caches the relevant information from the experiment
"""
function (t::AbstractHook)(::AbstractExperiment)
    error("functor for AbstractHook $(typeof(t)) not implemented")
end

"""
    when(t::AbstractHook)::CallTime

Returns when the hook should be called during an episode
"""
function when(t::AbstractHook)::CallTime
    error("when not implemented for AbstractHook $(typeof(t))")
end

"""
    name(t::AbstractHook)::AbstractString

Return the name of the hook, which is what the hooks data is saved as in the experiment
"""
function name(t::AbstractHook)::AbstractString
    error("name not implemented for AbstractHook $(typeof(t))")
end

"""
    data(t::AbstractHook)

Return the data stored by hook `t`
"""
function data(t::AbstractHook)
    error("data not implemented for AbstractHook $(typeof(t))")
end

function close!(t::AbstractHook, exp::AbstractExperiment)
    error("close! not implemented for AbstractHook $(typeof(t))")
end

"""
    save(s::AbstractExperimentSaver, e::AbstractExperiment, path::String)

Save the experimental data for experiment `e`
"""
function save(s::AbstractExperimentSaver, e::AbstractExperiment, path::String)
    error("save not implemented for AbstractExperimentSaver $(typeof(s))")
end
