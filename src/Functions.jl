function train! end

function eval! end

function close! end

"""
    continuous(x)::Bool

Return whether x is continuous
"""
function continuous end

"""
    discrete(x)::Bool

Return whether x is discrete
"""
function discrete end

function reset! end

"""
    unwrap(env::AbstractEnvironment)::AbstractEnvironment

Return the wrapped object (algo or environment).
"""
function unwrap end

"""
    wrapped(env::AbstractEnvironment)::AbstractEnvironment

Return the next wrapped object (algo or environment).
"""
function wrapped end

"""
    action(a::AbstractEnvironmentActionWrapper, action)
    action(a::AbstractAlgoActionWrapper, state, action)

Adjust the action `action` based on the action wrapper `a`.
"""
function action end

"""
    select_action(a::T, state).

Select an action in state `state` using learning algo `a` of type `T`.
"""
function select_action end
