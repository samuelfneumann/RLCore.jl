abstract type AbstractAlgoWrapper <: AbstractAlgo end

function wrapped(::AbstractAlgoWrapper)::AbstractAlgo
    return error("wrapped not implemented")
end

function unwrap(algo::AbstractAlgoWrapper)::AbstractAlgo
    return unwrap(wrapped(algo))
end

function algostep!(w::AbstractAlgoWrapper, state, action, reward, next_state, γ, done)
    return algostep!(wrapped(w), state, action, reward, next_state, γ, done)
end

function select_action(w::AbstractAlgoWrapper, state)
    return select_action(wrapped(w), state)
end

data(w::AbstractAlgoWrapper)::NamedTuple = data(wrapped(w))
eval!(w::AbstractAlgoWrapper) = eval!(wrapped(w))
train!(w::AbstractAlgoWrapper) = train!(wrapped(w))
reset!(w::AbstractAlgoWrapper) = reset!(wrapped(w))
istrain(w::AbstractAlgoWrapper) = istrain(wrapped(w))
iseval(w::AbstractAlgoWrapper) = !istrain(w)
close!(w::AbstractAlgoWrapper) = close!(wrapped(w))

# ###################################
# Action wrapper
# ###################################
"""
    AbstractAlgoActionWrapper <: AbstractAlgoWrapper

Wrap an algo to adjust the actions it selects

## Interface
In order to satisfy this interface and sub-type an AbstractAlgoActionWrapper, you
must implement the following functions:

    action(a::AbstractAlgoActionWrapper, state, action)
    wrapped(a::AbstractAlgoActionWrapper)
"""
abstract type AbstractAlgoActionWrapper <: AbstractAlgoWrapper end

function action(::AbstractAlgoActionWrapper, state, orig_action)
    error("action not implemented")
end

function select_action(w::AbstractAlgoActionWrapper, state)
    act = select_action(wrapped(w), state)
    return action(w, state, act)
end

