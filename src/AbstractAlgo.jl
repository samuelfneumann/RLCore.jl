"""
	AbstractAlgo

A learning algorithm
"""
abstract type AbstractAlgo end

"""
    step!(a::AbstractAlgo)

Take a single algo update step
"""
function step!(a::AbstractAlgo, state, action, reward, next_state, γ, done)
    if !istrain(a)
        return nothing
    end

    algostep!(a, state, action, reward, next_state, γ, done)
end

function select_action(a::AbstractAlgo, state)
    error("select_action not implemented")
end

function algostep!(::AbstractAlgo, state, action, reward, next_state, γ, done)
    error("algostep! not implemented")
end

wrapped(a::AbstractAlgo) = a
unwrap(a::AbstractAlgo) = a
data(a::AbstractAlgo)::NamedTuple = NamedTuple()
eval!(::AbstractAlgo) = error("eval! not implemented")
train!(::AbstractAlgo) = error("train! not implemented")
reset!(::AbstractAlgo) = error("reset! not implemented")
istrain(::AbstractAlgo) = error("istrain not implemented")
iseval(a::AbstractAlgo) = !istrain(a)
close!(::AbstractAlgo) = nothing
