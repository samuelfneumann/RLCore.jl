"""
    AbstractPolicy

AbstractPolicy implements an abstract policy for selecting actions in some environment
"""
abstract type AbstractPolicy end

function train!(::AbstractPolicy)
    error("train! not implemented")
end

function eval!(::AbstractPolicy)
    error("eval! not implemented")
end

"""
    continuous(::AbstractPolicy)::Bool

Return whether the policy is a continuous-action policy
"""
function continuous(::AbstractPolicy)::Bool
    error("continuous not implemented")
end

"""
    discrete(::AbstractPolicy)::Bool

Return whether the policy is a discrete-action policy
"""
function discrete(::AbstractPolicy)::Bool
    error("discrete not implemented")
end

function (p::AbstractPolicy)(args...; kwargs...)
    sample(p, args...; kwargs...)
end

"""
    Base.rand(p::AbstractPolicy, args...; kwargs...)

See `sample`.
"""
function Base.rand(p::AbstractPolicy, args...; kwargs...)
    return sample(p, args...; kwargs...)
end

function Base.rand(rng::AbstractRNG, p::AbstractPolicy, args...; kwargs...)
    return sample(p, rng, args...; kwargs...)
end

"""
    sample(::AbstractPolicy, [::AbstractRNG], states::AbstractArray; [num_samples::Int=1])

Sample `num_samples` actions from the policy in each state in `states`.
"""
function sample(
        ::AbstractPolicy,
        rng,
        states,
        num_samples::Int=1,
)
    error("sample not implemented")
end

function sample(
        p::AbstractPolicy,
        states,
        num_samples::Int=1,
)
    return sample(
        p,
        Xoshiro(),
        states;
        num_samples=num_samples,
    )
end

"""
    rsample(
        ::AbstractPolicy, [::AbstractRNG], states::AbstractArray; [num_samples::Int = 1])

Sample `num_samples` actions from the policy in each state in `states` using reparameterized
sampling.
"""
# TODO: return logprob from rsample as well
function rsample(
        ::AbstractPolicy,
        rng,
        states,
        num_samples::Int = 1,
)
    error("rsample not implemented")
end

function rsample(
        p::AbstractPolicy,
        states,
        num_samples::Int=1,
)
    return rsample(
        p,
        Xoshiro(),
        states;
        num_samples = num_samples,
    )
end

"""
    logprob(::AbstractPolicy, states::AbstractArray, actions::AbstractArray)
    logprob(::AbstractPolicy, states::AbstractArray)
    logprob(::AbstractPolicy, μ::AbstractArray, σ::AbstractArray, actions::AbstractArray)

Return the log probability of each action in each state. There should be exactly one action
in `actions` per state observation in `states`. `actions` should be either a 1- or
2-dimensional array depending on whether a single action or batch of actions is inputted
respectively.

If the policy is discrete, then calling this function with only state input returns the
log probability of each action in each state.
"""
function logprob(p::AbstractPolicy, states, actions::AbstractArray)
    error("logprob not implemented for policy $p")
end

function logprob(::AbstractPolicy, states::AbstractArray)
    error("logprob not implemented for policy $p")
end

function logprob(::AbstractPolicy, μ::AbstractArray, σ::AbstractArray, act::AbstractArray)
    error("logprob not implemented for policy $p")
end

Base.eltype(::AbstractPolicy) = error("eltype not implemented")
Base.in(item, ::AbstractPolicy) = error("in not implemented")
Base.contains(p::AbstractPolicy, item) = item in p

"""
    AbstractStaticPolicy <: AbstractPolicy

A policy with static distribution parameters. These parameters *cannot* be learned with
a function approximator.

## Interface
AbstractStaticPolicy subtypes should implement:
    sample(p::AbstractStaticPolicy, rng; num_samples)
    logprob(p::AbstractStaticPolicy, state, action)
"""
abstract type AbstractStaticPolicy <: AbstractPolicy end

function logprob(::AbstractStaticPolicy, state, action)
    error("logprob not implemented")
end

function rsample(::AbstractStaticPolicy, args...; kwargs...)
    error("cannot call rsample on AbstractStaticPolicy")
end

function train!(::AbstractStaticPolicy, args...; kwargs...)
    error("cannot call train! on AbstractStaticPolicy")
end

function eval!(::AbstractStaticPolicy, args...; kwargs...)
    error("cannot call eval! on AbstractStaticPolicy")
end
