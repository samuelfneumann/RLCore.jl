abstract type AbstractValueFunction end

function n_approximators(::AbstractValueFunction)::Int
    error("n_approximators not implemented")
end

function reduct(::AbstractValueFunction)::Function
    error("reduct not implemented")
end

function train!(::AbstractValueFunction)
    error("train! not implemented")
end

function eval!(::AbstractValueFunction)
    error("eval! not implemented")
end

function continuous(::AbstractValueFunction)::Bool
    error("continuous not implemented")
end

function discrete(::AbstractValueFunction)::Bool
    error("discrete not implemented")
end

function (::AbstractValueFunction)(args...)
    error("cannot call value function")
end

function loss(::AbstractValueFunction, args...)
end
