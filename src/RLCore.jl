module RLCore

export step!, continuous, discrete, close!, info, wrapped, unwrap

# Value Functions
export AbstractValueFunction
export n_approximators, reduct, loss

# Policies
export AbstractPolicy, AbstractStaticPolicy
export sample, rsample, logprob, all_logprob

# Algorithms
export AbstractAlgo, AbstractAlgoWrapper, AbstractAlgoActionWrapper
export select_action, reset!, train!, eval!, istrain, iseval, algostep!
# export update_actor!, update_critic!

# Spaces
export AbstractSpace, bounded, high, low, continuous, discrete

# Environments
export AbstractEnvironment, AbstractEnvironmentWrapper
export AbstractEnvironmentActionWrapper, AbstractEnvironmentObservationWrapper
export AbstractEnvironmentRewardWrapper
export action_space, observation_space, isterminal, reward, γ, envstep!

# Experiments
export AbstractExperiment, AbstractHook, TimeLog, StepsPerEpisode, ReturnPerEpisode
export AbstractExperimentEnder, AbstractExperimentSaver, ended, save
export CallTime, PreExperiment, PreEpisode, PreAct, PostAct, PostEpisode, PostExperiment
export data, when, run!, run_episode!, register!, expclose!
export algo, env, experiment, total_steps, total_episodes, data, hooks, name
export total_episodes, total_steps

@enum CallTime begin
    PreExperiment
    PreEpisode
    PreAct
    PostAct
    PostEpisode
    PostExperiment
end

using Random

include("Functions.jl")
include("AbstractAlgo.jl")
include("AbstractAlgoWrapper.jl")
include("AbstractSpace.jl")
include("AbstractPolicy.jl")
include("AbstractValueFunction.jl")
include("AbstractExperiment.jl")
include("AbstractEnvironment.jl")
include("AbstractEnvironmentWrapper.jl")
end
